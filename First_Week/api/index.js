const bull = require('bull');
//const { redis } = require('../settings');

const redis = { host: '192.168.89.27', port: 6379 };

const opts = {redis: {host: redis.host, port: redis.port}}

const queueCreate = bull('curso:create', opts);
const queueDelete = bull('curso:delete', opts);
const queueUpdate = bull('curso:update', opts);
const queueFindOne = bull('curso:findOne', opts);
const queueView = bull('curso:view', opts);

async function Create({ age, color, name }) {

    try {
        const job = await queueCreate.add({ age, color, name })

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function Delete({id}) {

    try {
        const job = await queueDelete.add({id})

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function Update({ age, color, name, id }) {

    try {
        const job = await queueUpdate.add({ age, color, name, id })

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function FindOnde({id}) {

    try {
        const job = await queueFindOne.add({id})

        const {statusCode, data, message} = await job.finished()

        /*if (statusCode === 200) {
            console.log("El usuario buscado es ", data)
        }

        else console.error(message)*/

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function View({}) {

    try {

        const job = await queueView.add({})

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function main() {
    //await Create({name: 'Laura', age: 30, color: 'Amarillo'})
    //await Delete({id: 4})
    //await Update({age: 24, id: 5})
    //await View({})
    //await FindOnde({id: 5})
}

//main()

module.exports = {Create, Update, Delete, FindOnde, View}
