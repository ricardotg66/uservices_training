import React, {useEffect, useState} from 'react';
const {io} = require('socket.io-client')


function App() {

  const [id, setId] = useState()
  const [data, setData] = useState([])

  useEffect(() => {

    const socket = io("http://192.168.89.27", {transports: ['websocket'], jsonp: false})

    setTimeout(() => setId(socket.id), 500)

    socket.on('res:microservice:view', ({statusCode, data, message}) => {
      console.log('res:microservice:view',{statusCode, data, message})

      console.log({statusCode, data, message})

      if (statusCode === 200) setData(data)
      
  })

  socket.on('res:microservice:create', ({statusCode, data, message}) => {
      console.log('res:microservice:create',{statusCode, data, message})
  })

  socket.on('res:microservice:findOne', ({statusCode, data, message}) => {
      console.log('res:microservice:findOne',{statusCode, data, message})
  })

  socket.on('res:microservice:update', ({statusCode, data, message}) => {
      console.log('res:microservice:update',{statusCode, data, message})
  })

  socket.on('res:microservice:delete', ({statusCode, data, message}) => {
      console.log('res:microservice:delete',{statusCode, data, message})
  })

  socket.emit('req:microservice:view', ({}))

  }, [])

  return (
    <div>
      
      <p>{id ? `Estas en línea ${id}` : 'Fuera de línea'}</p>

      {data.map((v, i) => <p key={i}>Nombre: {v.name}, Edad: {v.age} {v.worker}</p>)}

    </div>
  );
}

export default App;
