const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize')

const Model = sequelize.define('curso', {
    name: {type: DataTypes.STRING},
    age: {type: DataTypes.BIGINT},
    color: {type: DataTypes.STRING}
})

async function SyncDB() {
    try {
        await Model.sync({ logging: false})

        return { statusCode: 200, data: 'Ok'}

    } catch (error) {
        console.log(error)

        return { statusCode: 500, message: error.toString()}
    }
}

module.exports = { SyncDB, Model }