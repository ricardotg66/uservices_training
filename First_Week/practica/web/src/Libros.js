import React, {useState, useEffect} from "react";

import styled from 'styled-components'

import { socket } from "./ws";

const Container = styled.div`
    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: stretch;
    display: flex;
    width: 100%;
`
const Libro = styled.div`
    flex-direction: column;
    background-color: #303536;
    margin-bottom: 15px;
    border-radius: 10px;
    padding: 2px;
    display: flex;
    position: relative;
`

const Portada = styled.img`
    width: 200px;
`

const Icon = styled.img`
    width: 35px;
    height: 35px;
    position: absolute;
    top: -6px;
    right: -6px;
    cursor: pointer;
`

const Title = styled.p`
    color: white;
    text-align: center;
`

const Button = styled.button`
    background-color: green;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    border-radius: 15px;
    opacity: 0.8;
    color: white;
    width: 100%;
    height: 50px;
    margin-bottom: 25px;
`

const Item = ({ image, title, id }) => {

    const handleDelete = () => socket.emit('req:libros:delete', { id })

    const opts = {
        src: "https://cdn.pixabay.com/photo/2012/04/13/00/21/button-31222_960_720.png",
        onClick: handleDelete
    }

    
    return (
    <Libro>
        <Icon {...opts} />
        <Portada src = {image} />
        <Title>{title}</Title>
    </Libro>
    )
}

const App = () => {

    const [data, setData] = useState([])

    useEffect(() => {

        socket.on('res:libros:view', ({statusCode, data, message}) => {
          
          console.log('res:libros:view',{statusCode, data, message})
    
          console.log({statusCode, data, message})
    
          if (statusCode === 200) setData(data)
          
        })
    
        setTimeout(() => socket.emit('req:libros:view', ({})), 1000);
    
      }, []);

    const handleCreate = () => socket.emit('req:libros:create', { 
       image: 'https://cdn.pixabay.com/photo/2017/01/02/22/19/radio-1948011_960_720.jpg', 
       title: 'Aprende nodejs'
    })


    return (
        <Container>
            <Button onClick={handleCreate}>Create</Button>
            {data.map((v, i) => <Item {...v} />)}
        </Container>
    );

}

export default App;