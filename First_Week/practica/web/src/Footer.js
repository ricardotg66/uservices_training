import React, {useState, useEffect} from "react";
import { socket } from "./ws";

const App = () => {

    const [id, setId] = useState()

    useEffect(() => setTimeout(() => setId(socket.id), 500), [])

    return (
        <p>{id ? `Estas en línea ${id}` : 'Fuera de línea'}</p>
    )
}

export default App;