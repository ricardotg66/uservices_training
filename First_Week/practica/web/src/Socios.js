import React, {useState, useEffect} from "react";

import { socket } from "./ws";

import styled from 'styled-components';

const Container = styled.div`
    width: 300px;
    max-width: 300px;
`
const ContainerBody = styled.div`
    height: 350px;
    overflow: scroll;
`

const Socio = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 15px;
    position: relative;
`
const Body = styled.div`
    padding-left: 5px;
    padding-right: 5px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`
const Name = styled.p`
    color: #333;
`

const Phone = styled.p``

const Email = styled.p``

const Enable = styled.div`
    width: 25px;
    height: 25px;
    border-radius: 50%;
    opacity: 0.5;
    background-color: ${props => props.enable ? 'green' : 'red'};
    cursor: pointer;
`
const Button = styled.button`
    background-color: #f44336;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    padding-bottom: 10px;
    border-radius: 15px;
    opacity: 0.8;
    color: white;
    width: 100%;
    height: 50px;
    margin-bottom: 25px;
`

const Icon = styled.img`
    margin-left: 10px;
    width: 25px;
    height: 25px;
    cursor: pointer;
`

const Feedback = styled.div`
    align-items: center;
    display: flex;
    flex-direction: row;
`

const App = () => {

  const [data, setData] = useState([])
    
  useEffect(() => {

    socket.on('res:socios:view', ({statusCode, data, message}) => {
      console.log('res:socios:view',{statusCode, data, message})

      console.log({statusCode, data, message})

      if (statusCode === 200) setData(data)
      
    })

    setTimeout(() => socket.emit('req:socios:view', ({ })), 1000);

  }, []);

  const handleCreate = () => socket.emit('req:socios:create', { 
    name: 'Ricardo', phone: 8563181
  })

  const handleDelete = (id) => socket.emit('req:socios:delete', { id })

  const handleChange = (id, status) => {

    if (status) socket.emit('req:socios:disable', { id })

    else socket.emit('req:socios:enable', { id })
  }


  return (

    <Container> 
      
      <Button onClick={handleCreate}>Create</Button>

      <ContainerBody>
        {
          data.map((v, i) =>
            <Socio>
                    
              <Body>
                  <Name> {v.name} <small>{v.id}</small> </Name>

                  <Phone> {v.phone} </Phone>
              </Body>

              <Body>

                  <Email> {v.email} </Email>

                  <Feedback>
                    <Enable enable = {v.enable} onClick = {() => handleChange(v.id, v.enable)}/>
                    <Icon src="https://cdn.pixabay.com/photo/2012/04/13/00/21/button-31222_960_720.png" onClick={() => handleDelete(v.id)}/>
                  </Feedback>                   

              </Body>

          </Socio>
        )}

      </ContainerBody>

    </Container>
  )
}

export default App;