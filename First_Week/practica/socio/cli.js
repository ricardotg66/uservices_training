const api = require('./api')

async function main() {
    try {
        let {data, statusCode, message} = await api.Create({
            name: 'Ricardo José', phone: '01385313581'
        })

        console.log({data, statusCode, message})

    } catch (error) {
        console.log(error)
    }
    
}

async function View() {
    try {
        let {data, statusCode, message} = await api.View({enable: true})

        console.log({data, statusCode, message})

    } catch (error) {
        console.log(error)
    }
    
}

View()