const Services = require('../Services')

const {queueView, queueCreate, queueUpdate, queueDelete, queueFindOne, queueEnable, queueDisable} = require('./index')

async function View(job, done) {

    try {

        const { enable } = job.data;
        
        let {statusCode, data, message} = await Services.View({ enable });

        done(null, {statusCode, data: data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueView', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

    //throw new Error('Ocurrió un error inesperado');
}

async function Create(job, done) {

    try {

        const { name, phone } = job.data;
        
        let {statusCode, data, message} = await Services.Create({ name, phone });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueCreate', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function Update(job, done) {

    try {

        const { name, age, phone, email, id } = job.data;
        
        let {statusCode, data, message} = await Services.Update({ name, age, phone, email, id });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueUpdate', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function Delete(job, done) {

    try {

        const { id } = job.data;
        
        let {statusCode, data, message} = await Services.Delete({ id });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueDelete', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function FindOnde(job, done) {

    try {

        const { id } = job.data;
        
        let {statusCode, data, message} = await Services.FindOne({ id });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueFindOne', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function Enable(job, done) {

    try {

        const { id } = job.data;
        
        let {statusCode, data, message} = await Services.Enable({ id });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueEnable', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function Disable(job, done) {

    try {

        const { id } = job.data;
        
        let {statusCode, data, message} = await Services.Disable({ id });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueDisable', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function run() {
    try {

        console.log("Vamos a inicializar worker")

        queueView.process(View)
        queueCreate.process(Create)
        queueUpdate.process(Update)
        queueDelete.process(Delete)
        queueFindOne.process(FindOnde)
        queueEnable.process(Enable)
        queueDisable.process(Disable)

    } catch (error) {
        console.log(error)
    }
    
}

module.exports = { View, Create, Update, Delete, FindOnde, Enable, Disable, run }

