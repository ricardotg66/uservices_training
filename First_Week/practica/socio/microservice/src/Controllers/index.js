const { Model } = require('../Models')

async function Create({name, phone}) {
    try {
        let instance = await Model.create(
            {name, phone}, {logging: false}
        )

        return {statusCode: 200, data: instance.toJSON()}

    } catch (error) {
        console.log({step: 'Controller Create', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}

async function Delete({where = {}}) {
    try {
        let instance = await Model.destroy({where})

        return {statusCode: 200, data: 'OK'}
        
    } catch (error) {
        console.log({step: 'Controller Delete', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}

async function Update({phone, email, age, name, id}) {
    try {
        let instance = await Model.update(
            {phone, email, age, name, id},
            {where: {id}, logging: false, returning: true}
        )

        console.log(instance)

        return {statusCode: 200, data: instance[1][0].toJSON()}
        
    } catch (error) {
        console.log({step: 'Controller Update', error: error.toString()})

        return {statusCode: 500, message: "No existe el usuario"}
    }
    
}

async function FindOne({where = {}}) {
    try {
        let instance = await Model.findOne({where, logging: false})

        if (instance) return {statusCode: 200, data: instance.toJSON()}

        else return {statusCode: 400, message: error.toString()}
        
    } catch (error) {
        console.log({step: 'Controller FindOne', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}

async function View({where = {}}) {
    try {
        let instances = await Model.findAll({where, logging: false})

        return {statusCode: 200, data: instances}
        
    } catch (error) {
        console.log({step: 'Controller View', error: error.toString()})

        return {statusCode: 400, message: error.toString()}
    }
    
}

async function Enable({ id }) {
    try {

        let instance = await Model.update(
            {enable: true }, {where: {id}, logging: false, returning: true}
        )

        return {statusCode: 200, data: instance[1][0].toJSON()}
        
    } catch (error) {

        console.log({step: 'Controller Enable', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}

async function Disable({ id }) {
    try {
        let instance = await Model.update(
            {enable: false }, {where: {id}, logging: false, returning: true}
        )

        console.log(instance)

        return {statusCode: 200, data: instance[1][0].toJSON()}
        
    } catch (error) {
        console.log({step: 'Controller Disable', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}


module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable }