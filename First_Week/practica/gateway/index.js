const express = require('express')
const http = require('http')

const app = express()

const server = http.createServer(app)

const { Server } = require('socket.io')

const io = new Server(server)

const apiLibros = require('api-libros')
const apiPagos = require('api-pagos')
const apiSocio = require('api-socios')

server.listen(80, () => {
    console.log("Server initialize")

    io.on('connection', socket => {
        console.log("New conection", socket.id)

        // End-point libros

        socket.on('req:libros:view', async ({}) => {

            try {

                console.log('req:libros:view')

                const {statusCode, data, message} = await apiLibros.View({})

                return io.to(socket.id).emit('res:libros:view', {statusCode, data, message})

            } catch (error) {
                console.log(error)

            }            

        })

        socket.on('req:libros:create', async ({ title, image }) => {

            try {

                console.log('req:libros:create')

                const {statusCode, data, message} = await apiLibros.Create({ title, image})

                io.to(socket.id).emit('res:libros:create', {statusCode, data, message})

                const view = await apiLibros.View({})

                return io.to(socket.id).emit('res:libros:view', view)

            } catch (error) {
                console.log(error)
            }

        })

        socket.on('req:libros:update', async ({ category, section, title, id }) => {

            try {

                console.log('req:libros:update', ({ category, section, title, id }))

                const {statusCode, data, message} = await apiLibros.Update({ category, section, title, id })

                return io.to(socket.id).emit('res:libros:update', {statusCode, data, message})

            } catch (error) {
                console.log(error)
            }

        })

        socket.on('req:libros:delete', async ({id}) => {

            try {

                console.log('req:libros:delete', ({id}))

                const {statusCode, data, message} = await apiLibros.Delete({ id })

                io.to(socket.id).emit('res:libros:delete', {statusCode, data, message})

                const view = await apiLibros.View({})

                io.to(socket.id).emit('res:libros:view', view)

            } catch (error) {
                console.log(error)
            }


        })

        socket.on('req:libros:findOne', async ({title}) => {

            try {

                console.log('req:libros:findOne', ({title}))

                const {statusCode, data, message} = await apiLibros.FindOnde({ title })

                return io.to(socket.id).emit('res:libros:findOne', {statusCode, data, message})

            } catch (error) {
                console.log(error)
            }

        })

        // End-point pagos

        socket.on('req:pagos:view', async ({}) => {

            try {

                console.log('req:pagos:view')

                const {statusCode, data, message} = await apiPagos.View({})

                return io.to(socket.id).emit('res:pagos:view', {statusCode, data, message})

            } catch (error) {
                console.log(error)

            }            

        })

        socket.on('req:pagos:create', async ({ socio, amount }) => {

            try {

                console.log('req:pagos:create', ({ socio, amount }))

                const {statusCode, data, message} = await apiPagos.Create({ socio, amount })

                io.to(socket.id).emit('res:pagos:create', {statusCode, data, message})

                const view = await apiPagos.View({})

                return io.to(socket.id).emit('res:pagos:view', view)

            } catch (error) {
                console.log(error)
            }

        })

        socket.on('req:pagos:delete', async ({id}) => {

            try {

                console.log('req:pagos:delete', ({id}))

                const {statusCode, data, message} = await apiPagos.Delete({ id })

                io.to(socket.id).emit('res:pagos:delete', {statusCode, data, message})

                const view = await apiPagos.View({})

                return io.to(socket.id).emit('res:pagos:view', view)

            } catch (error) {
                console.log(error)
            }


        })

        socket.on('req:pagos:findOne', async ({id}) => {

            try {

                console.log('req:pagos:findOne', ({id}))

                const {statusCode, data, message} = await apiPagos.FindOnde({ id })

                return io.to(socket.id).emit('res:pagos:findOne', {statusCode, data, message})

            } catch (error) {
                console.log(error)
            }

        })

        // End-point socios

        socket.on('req:socios:view', async ({enable}) => {

            try {

                console.log('req:socios:view')

                const {statusCode, data, message} = await apiSocio.View({ enable })

                return io.to(socket.id).emit('res:socios:view', {statusCode, data, message})

            } catch (error) {
                console.log(error)

            }            

        })

        socket.on('req:socios:create', async ({ phone, name }) => {

            try {

                console.log('req:socios:create', ({ phone, name }))

                const {statusCode, data, message} = await apiSocio.Create({ phone, name })

                io.to(socket.id).emit('res:socios:create', {statusCode, data, message})

                const view = await apiSocio.View({ })

                return io.to(socket.id).emit('res:socios:view', view)

            } catch (error) {
                console.log(error)
            }

        })

        socket.on('req:socios:update', async ({ age, name, id, phone }) => {

            try {

                console.log('req:socios:update', ({ age, name, id, phone }))

                const {statusCode, data, message} = await apiSocio.Update({ age, name, id, phone })

                return io.to(socket.id).emit('res:socios:update', {statusCode, data, message})

            } catch (error) {
                console.log(error)
            }

        })

        socket.on('req:socios:delete', async ({id}) => {

            try {

                console.log('req:socios:delete', ({id}))

                const {statusCode, data, message} = await apiSocio.Delete({ id })

                io.to(socket.id).emit('res:socios:delete', {statusCode, data, message})

                const view = await apiSocio.View({ })

                return io.to(socket.id).emit('res:socios:view', view)

            } catch (error) {
                console.log(error)
            }


        })

        socket.on('req:socios:findOne', async ({id}) => {

            try {

                console.log('req:socios:findOne', ({id}))

                const {statusCode, data, message} = await apiSocio.FindOnde({ id })

                return io.to(socket.id).emit('res:socios:findOne', {statusCode, data, message})

            } catch (error) {
                console.log(error)
            }

        })

        socket.on('req:socios:enable', async ({id}) => {

            try {

                console.log('req:socios:enable', ({id}))

                const {statusCode, data, message} = await apiSocio.Enable({ id })

                io.to(socket.id).emit('res:socios:enable', {statusCode, data, message})

                const view = await apiSocio.View({ })

                return io.to(socket.id).emit('res:socios:view', view)

            } catch (error) {
                console.log(error)
            }

        })

        socket.on('req:socios:disable', async ({id}) => {

            try {

                console.log('req:socios:disable', ({id}))

                const {statusCode, data, message} = await apiSocio.Disable({ id })

                io.to(socket.id).emit('res:socios:disable', {statusCode, data, message})

                const view = await apiSocio.View({ })

                return io.to(socket.id).emit('res:socios:view', view)

            } catch (error) {
                console.log(error)
            }

        })

    })
})