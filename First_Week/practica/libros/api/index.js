const {name} = require('./package.json')
const bull = require('bull');

const redis = { host: '192.168.0.115', port: 6379 };

const opts = {redis: {host: redis.host, port: redis.port}}

const queueCreate = bull(`${name.replace('api-', '')}:create`, opts);
const queueDelete = bull(`${name.replace('api-', '')}:delete`, opts);
const queueUpdate = bull(`${name.replace('api-', '')}:update`, opts);
const queueFindOne = bull(`${name.replace('api-', '')}:findOne`, opts);
const queueView = bull(`${name.replace('api-', '')}:view`, opts);

async function Create({ title, image }) {

    try {
        const job = await queueCreate.add({ title, image })

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function Delete({id}) {

    try {
        const job = await queueDelete.add({id})

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function Update({ category, section, title, id }) {

    try {
        const job = await queueUpdate.add({ category, section, title, id })

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function FindOnde({title}) {

    try {
        const job = await queueFindOne.add({title})

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function View({}) {

    try {

        const job = await queueView.add({})

        const {statusCode, data, message} = await job.finished()

        return {statusCode, data, message}

    } catch (error) {
        console.log(error)
    }
}

async function main() {
    //await Create({name: 'Laura', age: 30, color: 'Amarillo'})
    //await Delete({id: 4})
    //await Update({age: 24, id: 5})
    //await View({})
    //await FindOnde({id: 5})
}

//main()

module.exports = {Create, Update, Delete, FindOnde, View}
