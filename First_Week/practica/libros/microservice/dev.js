const micro = require('./src')

async function main() {
    try {

        console.log("Vamos a inicializar la base de datos")

        const db = await micro.SyncDB()

        console.log("Base de datos iniciada")

        if (db.statusCode !== 200) throw db.message

        await micro.run()

    } catch (error) {
        console.log(error)
    }
    
}

main()