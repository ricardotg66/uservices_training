const { sequelize } = require('../settings')
const { DataTypes } = require('sequelize')
const {name} = require('../../package.json')

const Model = sequelize.define(name, {
    title: {type: DataTypes.STRING},
    category: {type: DataTypes.BIGINT},
    section: {type: DataTypes.STRING},
    image: {type: DataTypes.STRING}
})

async function SyncDB() {
    try {
        await Model.sync({ logging: false, force: true})

        return { statusCode: 200, data: 'Ok'}

    } catch (error) {
        console.log(error)

        return { statusCode: 500, message: error.toString()}
    }
}

module.exports = { SyncDB, Model }