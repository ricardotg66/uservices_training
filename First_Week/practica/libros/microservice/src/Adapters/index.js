const bull = require('bull');
const { redis } = require('../settings');

const {name} = require('../../package.json')

const opts = {redis: {host: redis.host, port: redis.port}}

const queueCreate = bull(`${name}:create`, opts);
const queueDelete = bull(`${name}:delete`, opts);
const queueUpdate = bull(`${name}:update`, opts);
const queueFindOne = bull(`${name}:findOne`, opts);
const queueView = bull(`${name}:view`, opts);

module.exports = {queueView, queueCreate, queueDelete, queueFindOne, queueUpdate}