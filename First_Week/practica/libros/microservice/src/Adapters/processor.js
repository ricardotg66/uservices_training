const Services = require('../Services')

const {queueView, queueCreate, queueUpdate, queueDelete, queueFindOne} = require('./index')

async function View(job, done) {

    try {

        const { } = job.data;

        console.log(job.id)
        
        let {statusCode, data, message} = await Services.View({ });

        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueView', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

    //throw new Error('Ocurrió un error inesperado');
}

async function Create(job, done) {

    try {

        const { title, image } = job.data;
        
        let {statusCode, data, message} = await Services.Create({ title, image });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueCreate', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function Update(job, done) {

    try {

        const { category, section, title, id } = job.data;
        
        let {statusCode, data, message} = await Services.Update({ category, section, title, id });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueUpdate', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function Delete(job, done) {

    try {

        const { id } = job.data;
        
        let {statusCode, data, message} = await Services.Delete({ id });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueDelete', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function FindOnde(job, done) {

    try {

        const { title } = job.data;
        
        let {statusCode, data, message} = await Services.FindOne({ title });
        done(null, {statusCode, data, message}); 

    } catch (error) {

        console.log({step: 'adapter queueFindOne', error: error.toString()})
        done(null, {statusCode: 500, message: internalError}) 

    }

}

async function run() {
    try {

        console.log("Vamos a inicializar worker")

        queueView.process(View)
        queueCreate.process(Create)
        queueUpdate.process(Update)
        queueDelete.process(Delete)
        queueFindOne.process(FindOnde)

    } catch (error) {
        console.log(error)
    }
    
}

module.exports = { View, Create, Update, Delete, FindOnde, run }

