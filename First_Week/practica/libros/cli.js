const api = require('./api')

async function main() {
    try {
        let {data, statusCode, message} = await api.Create({
            image: 'https://cdn.pixabay.com/photo/2017/01/02/22/19/radio-1948011_960_720.jpg', title: 'Aprende nodejs'
        })

        console.log({data, statusCode, message})

    } catch (error) {
        console.log(error)
    }
    
}

main()