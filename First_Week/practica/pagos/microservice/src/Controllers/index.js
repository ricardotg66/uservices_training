const { Model } = require('../Models')

async function Create({socio, amount}) {
    try {
        let instance = await Model.create(
            {socio, amount}, {logging: false}
        )

        return {statusCode: 200, data: instance.toJSON()}

    } catch (error) {
        console.log({step: 'Controller Create', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}

async function Delete({where = {}}) {
    try {
        let instance = await Model.destroy({where})

        return {statusCode: 200, data: 'OK'}
        
    } catch (error) {
        console.log({step: 'Controller Delete', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}

async function FindOne({where = {}}) {
    try {
        let instance = await Model.findOne({where, logging: false})

        if (instance) return {statusCode: 200, data: instance.toJSON()}

        else return {statusCode: 400, message: error.toString()}
        
    } catch (error) {
        console.log({step: 'Controller FindOne', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }
    
}

async function View({where = {}}) {
    try {
        let instances = await Model.findAll({where, logging: false})

        return {statusCode: 200, data: instances}
        
    } catch (error) {
        console.log({step: 'Controller View', error: error.toString()})

        return {statusCode: 400, message: error.toString()}
    }
    
}

module.exports = { Create, Delete, FindOne, View }