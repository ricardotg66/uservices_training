const bull = require('bull');
const {name} = require('../../package.json')
const { redis } = require('../settings');

const opts = {redis: {host: redis.host, port: redis.port}}

const queueCreate = bull(`${name}:create`, opts);
const queueDelete = bull(`${name}:delete`, opts);
const queueFindOne = bull(`${name}:findOne`, opts);
const queueView = bull(`${name}:view`, opts);

module.exports = {queueView, queueCreate, queueDelete, queueFindOne}