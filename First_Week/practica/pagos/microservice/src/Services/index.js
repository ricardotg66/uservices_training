const Controllers = require('../Controllers')
const { internalError, redis } = require('../settings')
const socios = require('api-socios')

async function Create({ socio, amount }) {

    try {

        const validarSocio = await socios.FindOnde({id: socio}, redis)

        console.log(validarSocio)

        if (validarSocio.statusCode !== 200) {

            switch (validarSocio.statusCode) {

                case 400: {
                    return {statusCode: 400, message: "No existe el usuario"}
                }
            
                default: return {statusCode: 400, message: internalError}
            }
        }

        if (!validarSocio.data.enable) {
            
            return {statusCode: 400, message: "EL socio no esta habilitado"}
        }
        
        let {statusCode, data, message} = await Controllers.Create({ socio, amount })

        return {statusCode, data, message}       
        
    } catch (error) {

        console.log({step: 'service Create', error: error.toString()})

        return {statusCode: 500, message: error.toString()}
    }      
}

async function Delete({ id }) {
    try {

        const findOne = await Controllers.FindOne({where: {id}})

        if (findOne.statusCode !== 200) {

            switch (findOne.statusCode) {

                case 400: return {statusCode: 400, message: "No existe el usuario a eliminar"}
                    
                default: return {statusCode: 500, message: internalError}

            }


        }
        
        const del = await Controllers.Delete({ where: {id}})

        if (del.statusCode === 200) return {statusCode: 200, data: findOne.data}
        
        return {statusCode: 400, message: internalError}      
        
    } catch (error) {

        console.log({step: 'service Delete', error: error.toString()})
        
        return {statusCode: 500, message: error.toString()}
    }      
}

async function FindOne({id}) {
    try {
        
        let {statusCode, data, message} = await Controllers.FindOne({where: {id}})

        return {statusCode, data, message}       
        
    } catch (error) {

        console.log({step: 'service FindOnde', error: error.toString()})
        
        return {statusCode: 500, message: error.toString()}
    }      
}

async function View({ }) {
    try {
        
        let {statusCode, data, message} = await Controllers.View({ })

        return {statusCode, data, message}       
        
    } catch (error) {

        console.log({step: 'service View', error: error.toString()})
        
        return {statusCode: 500, message: error.toString()}
    }      
}

module.exports = {Create, Delete, FindOne, View}