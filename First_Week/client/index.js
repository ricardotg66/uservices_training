const {io} = require('socket.io-client')

const socket = io("http://192.168.89.27", {port: 80}) //, {transports: ['websocket']}

async function main() {
    try {
        setTimeout(() => console.log(socket.id), 500)

        socket.on('res:microservice:view', ({statusCode, data, message}) => {
            console.log('res:microservice:view',{statusCode, data, message})
        })

        socket.on('res:microservice:create', ({statusCode, data, message}) => {
            console.log('res:microservice:create',{statusCode, data, message})
        })

        socket.on('res:microservice:findOne', ({statusCode, data, message}) => {
            console.log('res:microservice:findOne',{statusCode, data, message})
        })

        socket.on('res:microservice:update', ({statusCode, data, message}) => {
            console.log('res:microservice:update',{statusCode, data, message})
        })

        socket.on('res:microservice:delete', ({statusCode, data, message}) => {
            console.log('res:microservice:delete',{statusCode, data, message})
        })

        //setInterval(() => socket.emit('req:microservice:view', ({})), 500)

        setTimeout(() => {
            socket.emit('req:microservice:create', {age: 21, name: 'Marcela', color: 'azul'})

            //socket.emit('req:microservice:view', ({}))
            //socket.emit('req:microservice:delete', ({id: 5}))
            //socket.emit('req:microservice:update', ({id: 6, age: 30}))
            //socket.emit('req:microservice:findOne', ({id: 6}))

        }, 300)

        

    } catch (error) {
        console.log(error)
    }
    
}

main()